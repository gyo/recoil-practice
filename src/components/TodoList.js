import React from "react";
import { useRecoilValue } from "recoil";

import { TodoItemCreator } from "./TodoItemCreator";
import { TodoItem } from "./TodoItem";
import { TodoListFilters } from "./TodoListFilters";
import { TodoListStats } from "./TodoListStats";
import { filteredTodoListState } from "../state/filteredTodoListState";

export const TodoList = () => {
  const todoList = useRecoilValue(filteredTodoListState);

  console.log([...todoList]);

  return (
    <>
      <TodoListStats />
      <TodoListFilters />
      <TodoItemCreator />

      {todoList.map((todoItem) => (
        <TodoItem key={todoItem.id} item={todoItem} />
      ))}
    </>
  );
};
