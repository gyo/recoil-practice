import React, { useState, useCallback } from "react";
import { useSetRecoilState } from "recoil";

import { todoListState } from "../state/todoListState";

export const TodoItemCreator = () => {
  const [inputValue, setInputValue] = useState("");
  const setTodoList = useSetRecoilState(todoListState);

  const onChange = useCallback(({ target: { value } }) => {
    setInputValue(value);
  }, []);

  const addItem = useCallback((e) => {
    e.preventDefault();

    setTodoList((oldTodoList) => [
      ...oldTodoList,
      {
        id: getId(),
        text: inputValue,
        isComplete: false,
      },
    ]);
    setInputValue("");
  }, [inputValue, setTodoList]);

  return (
    <form onSubmit={addItem}>
      <input type="text" value={inputValue} onChange={onChange} />
      <button type="submit">Add</button>
    </form>
  );
};

// utility for creating unique Id
let id = 0;
const getId = () => {
  return id++;
};
