import React, { useCallback } from "react";
import { useRecoilState } from "recoil";

import { todoListFilterState } from "../state/todoListFilterState";

export const TodoListFilters = () => {
  const [filter, setFilter] = useRecoilState(todoListFilterState);

  const updateFilter = useCallback(
    ({ target: { value } }) => {
      setFilter(value);
    },
    [setFilter]
  );

  return (
    <>
      Filter:
      <label>
        <input
          type="radio"
          name="todoListFilterState"
          value="Show All"
          checked={filter === "Show All"}
          onChange={updateFilter}
        ></input>
        All
      </label>
      <label>
        <input
          type="radio"
          name="todoListFilterState"
          value="Show Completed"
          checked={filter === "Show Completed"}
          onChange={updateFilter}
        ></input>
        Completed
      </label>
      <label>
        <input
          type="radio"
          name="todoListFilterState"
          value="Show Uncompleted"
          checked={filter === "Show Uncompleted"}
          onChange={updateFilter}
        ></input>
        Uncompleted
      </label>
    </>
  );
};
